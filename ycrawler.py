import asyncio
import aiohttp
import re
import os
from uuid import uuid4
from html import unescape
import logging


NEWS_URL = "https://news.ycombinator.com/"
COMMENTS_URL = "https://news.ycombinator.com/item?id={}"
BASE_DIR = 'NEWS'
NEWS_LINK_PATTERN = re.compile(b'id=\'(\d+)\'>[\s\S]*?<a href=\"(.*?)\" class=\"storylink\">[\s\S]*?</a>')
COMMENTS_LINK_PATTERN = re.compile(b'.*<a href="(htt.*)" rel="nofollow">http')
USER_AGENT = "Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:70.0) Gecko/20100101 Firefox/70.0"
CONNECT_TIMEOUT = 30
SLEEP_INTERVAL = 15


def downloaded_pages():
    return os.listdir(BASE_DIR)


async def fetch(session, url):
    try:
        async with session.get(url, verify_ssl=False) as response:
            return await response.read()
    except Exception as e:
        logging.info(f'{url}: {e}')


async def get_news_urls(session):
    response = await fetch(session, NEWS_URL)
    return [(url[0].decode('UTF-8'), url[1].decode('UTF-8')) for url in NEWS_LINK_PATTERN.findall(response)]


async def download_comment(session, url, news_id):
    page = await fetch(session, unescape(url.decode('UTF-8')))
    if page:
        loop = asyncio.get_running_loop()
        await loop.run_in_executor(
            None,
            save_page,
            page, news_id, str(uuid4())
        )


async def download_all(session, url, news_id):
    news_page = await fetch(session, url)
    if news_page:
        loop = asyncio.get_running_loop()
        await loop.run_in_executor(
            None,
            save_page,
            news_page, news_id, news_id
        )
        comments_page = await fetch(session, COMMENTS_URL.format(news_id))
        comments = COMMENTS_LINK_PATTERN.findall(comments_page)
        tasks = [download_comment(session, url, news_id) for url in comments]
        await asyncio.gather(*tasks)


def save_page(content, path, filename):
    page_dir = os.path.join(BASE_DIR, path)
    if not os.path.exists(page_dir):
        os.mkdir(page_dir)
    with open(os.path.join(page_dir, filename), 'wb') as f:
        f.write(content)


async def main():
    async with aiohttp.ClientSession(
            headers={'user-agent': USER_AGENT},
            timeout=aiohttp.ClientTimeout(total=CONNECT_TIMEOUT),
    ) as session:
        while True:
            news_urls = await get_news_urls(session)
            for comment_id, url in news_urls:
                if 'item?id=' in url:
                    url = NEWS_URL + url
                if comment_id not in downloaded_pages():
                    asyncio.create_task(download_all(session, url, comment_id))
            await asyncio.sleep(SLEEP_INTERVAL)


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO,
                        format='[%(asctime)s] %(levelname).1s %(message)s',
                        datefmt='%Y.%m.%d %H:%M:%S')
    os.makedirs(BASE_DIR, exist_ok=True)
    try:
        asyncio.run(main())
    except KeyboardInterrupt:
        logging.info('Interrupted by user')
